﻿using Microsoft.Xrm.Tooling.Connector;
using System;

namespace Omnipresence.Dynamics
{
    public sealed class DynamicsServiceClient
    {
        private static readonly Lazy<DynamicsServiceClient>
            lazy = new Lazy<DynamicsServiceClient>
                (() => new DynamicsServiceClient());

        public static DynamicsServiceClient Instance { get { return lazy.Value; } }

        public static CrmServiceClient ServiceClient;

        private DynamicsServiceClient()
        {
            string connectionString = "AuthType=ClientSecret; ClientId=3115bfd4-47de-4baa-9162-0292824ba9f6; ClientSecret=zJD3O/J?=gPfSjMPEd9zc4-t1cK:Ua-U; Url=https://jjmomnipresence.crm.dynamics.com/";
            CrmServiceClient.MaxConnectionTimeout = new TimeSpan(0, 10, 0);
            ServiceClient = new CrmServiceClient(connectionString);
        }

        public CrmServiceClient GetCrmServiceClient()
        {
            return ServiceClient;
        }
    }
}
