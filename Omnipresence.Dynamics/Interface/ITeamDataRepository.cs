﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;
using Omnipresence.Dynamics.Model;
using System;


namespace Omnipresence.Dynamics.Interface
{
    public static class ITeamDataRepository
    {
        public static bool CreateOrUpdateAsync(TeamDataEntity teamDataEntity)
        {
            CrmServiceClient crmServiceClient = DynamicsServiceClient.Instance.GetCrmServiceClient();

            Entity teams = new Entity("team");
            teams.Attributes["ind_teamsserviceurl"] = teamDataEntity.ServiceUrl;
            teams.Attributes["ind_teamstenentid"] = teamDataEntity.TenantId;
            teams.Attributes["ind_teamid"] = teamDataEntity.TeamId;
            teams.Attributes["ind_channelid"] = teamDataEntity.ChannelId;
            teams.Attributes["name"] = teamDataEntity.Name;

            teams.Attributes["administratorid"] = new EntityReference("systemuser", new Guid("B0B59B35-D13D-EA11-A812-000D3A8C9000"));
            teams.Attributes["teamtype"] = new OptionSetValue(0);
            teams.Attributes["businessunitid"] = new EntityReference("businessunit",new Guid("38BFB683-90AC-E911-A836-000D3A1D5DCC"));

            Guid createdId = new Guid();
            try
            {
                createdId = crmServiceClient.Create(teams);
            }
            catch (Exception ex)
            {

            }

           

            if (createdId != null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
