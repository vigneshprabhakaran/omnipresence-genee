﻿using AdaptiveCards;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using Omnipresence.Dynamics.AdaptiveCardHelper;
using Omnipresence.Dynamics.Model;
using System;
using System.Collections.Generic;

namespace Omnipresence.Dynamics.Interface
{
    public class IApprovalActivity
    {
        private readonly CrmServiceClient _crmServiceClient;
        private readonly AdaptiveCardCreator _adaptiveCardCreator;

        public IApprovalActivity()
        {
            _crmServiceClient = DynamicsServiceClient.Instance.GetCrmServiceClient();
            _adaptiveCardCreator = new AdaptiveCardCreator();
        }

        public TeamsCardPayload GetApprovalAdaptiveCard(Guid approvalActivityId)
        {
            indskr_approvalactivity approvalActivity = GetApprovalActivityDetails(approvalActivityId);

            string owneridtype = ((AliasedValue)approvalActivity["Approver.owneridtype"]).Value.ToString();

            Guid ownerid = (Guid)((AliasedValue)approvalActivity["Approver.ownerid"]).Value;

            if (owneridtype == "systemuser")
            {
                UserDataEntity userDataEntity = GetUserDetails(ownerid);

                AdaptiveCard adaptiveCard = ComposeAdaptiveCardForPersonal(approvalActivity);

                TeamsCardPayload teamsCardPayload = new TeamsCardPayload()
                {
                    AdaptiveCardPayload = adaptiveCard,
                    UserDataEntity = userDataEntity,
                    IsChannelApproval = false
                };

                return teamsCardPayload;
            }
            else
            {
                TeamDataEntity teamDataEntity = GetTeamsDetails(ownerid);

                AdaptiveCard adaptiveCard = ComposeAdaptiveCardForTeams(approvalActivity);

                TeamsCardPayload teamsCardPayload = new TeamsCardPayload()
                {
                    AdaptiveCardPayload = adaptiveCard,
                    TeamDataEntity = teamDataEntity,
                    IsChannelApproval = true
                };

                return teamsCardPayload;
            }

        }


        #region Private Methods
        private indskr_approvalactivity GetApprovalActivityDetails(Guid approvalActivityId)
        {
            var QEindskr_approvalactivity = new QueryExpression("indskr_approvalactivity");

            QEindskr_approvalactivity.ColumnSet.AddColumns("indskr_name", "indskr_approvalactivityid", "indskr_cycle", "indskr_approvalrequired", "indskr_autoapproval", "indskr_targetentityid", "indskr_targetentityname", "indskr_approvalsequence", "indskr_approver", "statuscode", "indskr_approvalitem");

            QEindskr_approvalactivity.Criteria.AddCondition("indskr_approvalactivityid", ConditionOperator.Equal, approvalActivityId);

            var QEindskr_approvalactivity_indskr_approver_type = QEindskr_approvalactivity.AddLink("indskr_approver_type", "indskr_approver", "indskr_approver_typeid");

            var QEindskr_approvalactivity_indskr_approver_type_owner = QEindskr_approvalactivity_indskr_approver_type.AddLink("owner", "ownerid", "ownerid");
            QEindskr_approvalactivity_indskr_approver_type_owner.EntityAlias = "Approver";

            QEindskr_approvalactivity_indskr_approver_type_owner.Columns.AddColumns("owneridtype", "ownerid");

            indskr_approvalactivity approvalActivity = _crmServiceClient.RetrieveMultiple(QEindskr_approvalactivity).Entities[0].ToEntity<indskr_approvalactivity>();
            return approvalActivity;
        }

        private AdaptiveCard ComposeAdaptiveCardForPersonal(indskr_approvalactivity approvalActivity)
        {
            ApprovalActivityPayload activityPayload = new ApprovalActivityPayload
            {
                ApprovalActivityId = approvalActivity.Id.ToString(),
                Name = approvalActivity.indskr_name,
                Cycle = approvalActivity.indskr_Cycle?.ToString(),
                ApprovalSequence = approvalActivity.indskr_ApprovalSequence?.ToString(),
                Approver = approvalActivity.indskr_Approver.Name,
                ApprovalMandatory = (bool)approvalActivity.indskr_ApprovalRequired,
                ExpireIn = approvalActivity.indskr_AutoApproval?.ToString(),
                Target = approvalActivity.indskr_TargetEntityName,
                TargetId = approvalActivity.indskr_TargetEntityId
            };

            AdaptiveCard adaptiveCard = _adaptiveCardCreator.CreatePersonalAdaptiveCard(activityPayload);
            return adaptiveCard;
        }

        private TeamDataEntity GetTeamsDetails(Guid ownerid)
        {
            Entity entity = _crmServiceClient.Retrieve("team", ownerid, new ColumnSet("ind_teamsserviceurl", "ind_teamstenentid", "ind_teamid", "ind_channelid"));

            TeamDataEntity teamDataEntity = new TeamDataEntity()
            {
                ServiceUrl = entity.GetAttributeValue<string>("ind_teamsserviceurl"),
                TenantId = entity.GetAttributeValue<string>("ind_teamstenentid"),
                TeamId = entity.GetAttributeValue<string>("ind_teamid"),
                ChannelId = entity.GetAttributeValue<string>("ind_channelid"),
            };
            return teamDataEntity;
        }

         private UserDataEntity GetUserDetails(Guid ownerid)
        {
            Entity entity = _crmServiceClient.Retrieve("systemuser", ownerid, new ColumnSet("ind_teamsserviceurl", "ind_teamsazureid", "ind_teamsuserid", "ind_teamsconversationid", "ind_teamstenantid"));

            UserDataEntity userDataEntity = new UserDataEntity()
            {
                AadId = entity.GetAttributeValue<string>("ind_teamsazureid"),
                ConversationId = entity.GetAttributeValue<string>("ind_teamsconversationid"),
                ServiceUrl = entity.GetAttributeValue<string>("ind_teamsserviceurl"),
                TenantId = entity.GetAttributeValue<string>("ind_teamstenantid"),
                UserId = entity.GetAttributeValue<string>("ind_teamsuserid")
            };
            return userDataEntity;
        }

        private AdaptiveCard ComposeAdaptiveCardForTeams(indskr_approvalactivity approvalActivity)
        {

            var osValue = approvalActivity.indskr_ApprovalItem.Value;
            AdaptiveCard adaptiveCard;
            switch (osValue)
            {
                case 121430000:
                    adaptiveCard = PresentationTeamsAdaptiveCard(approvalActivity);
                    break;
                case 548910013:
                    adaptiveCard = QuoteTeamsAdaptiveCard(approvalActivity);
                    break;
                default:
                    adaptiveCard = null;
                    break;
            }


            return adaptiveCard;
        }

        private AdaptiveCard QuoteTeamsAdaptiveCard(indskr_approvalactivity approvalActivity)
        {
            Entity entity = _crmServiceClient.Retrieve("quote", new Guid(approvalActivity.indskr_TargetEntityId), new ColumnSet("name",
                "discountamount",
                "totallineitemamount",
                "totalamount",
                "totaltax",
                "quotenumber",
                "ind_type",
                "revisionnumber",
                "pricelevelid",
                "createdby",
                "transactioncurrencyid"));


            QuoteAdaptivePayload quoteAdaptive = new QuoteAdaptivePayload()
            {
                TargetId = approvalActivity.indskr_TargetEntityId,
                TargetEntity = approvalActivity.indskr_TargetEntityName,
                Notification = approvalActivity.indskr_name,
                Name = entity.GetAttributeValue<string>("name"),
                QuoteId = entity.GetAttributeValue<string>("quotenumber"),
                PriceList = entity.GetAttributeValue<EntityReference>("pricelevelid")?.Name,
                RevisionId = entity.GetAttributeValue<int>("revisionnumber").ToString(),
                Currency = entity.GetAttributeValue<EntityReference>("transactioncurrencyid")?.Name,
                CreatedBy = entity.GetAttributeValue<EntityReference>("createdby")?.Name,
                TotalAmount = entity.GetAttributeValue<Money>("totalamount")?.Value.ToString(),
                DiscountAmount = entity.GetAttributeValue<Money>("discountamount")?.Value.ToString(),
                TotalTax = entity.GetAttributeValue<Money>("totaltax")?.Value.ToString(),
                TotalDetailAmount = entity.GetAttributeValue<Money>("totallineitemamount")?.Value.ToString(),
            };

            AdaptiveCard adaptiveCard = _adaptiveCardCreator.CreateTeamsQuoteAdaptiveCard(quoteAdaptive);
            return adaptiveCard;

        }

        private AdaptiveCard PresentationTeamsAdaptiveCard(indskr_approvalactivity approvalActivity)
        {
            PresentationAdaptivePayload activityPayload = new PresentationAdaptivePayload
            {
                Name = approvalActivity.indskr_name,
                TargetId = approvalActivity.indskr_TargetEntityId,
                TargetEntity = approvalActivity.indskr_TargetEntityName
            };

            var QEindskr_iopresentation = new QueryExpression("indskr_iopresentation");

            QEindskr_iopresentation.ColumnSet.AddColumns("indskr_ckmzipurl", "indskr_minorversion", "indskr_majorversion", "createdby");

            QEindskr_iopresentation.Criteria.AddCondition("indskr_iopresentationid", ConditionOperator.Equal, approvalActivity.indskr_TargetEntityId);

            var QEindskr_iopresentation_indskr_iopage = QEindskr_iopresentation.AddLink("indskr_iopage", "indskr_iopresentationid", "indskr_iopresentationid");
            QEindskr_iopresentation_indskr_iopage.EntityAlias = "Pages";

            QEindskr_iopresentation_indskr_iopage.Columns.AddColumns("indskr_ckmpagethumbnailurl");


            EntityCollection entities = _crmServiceClient.RetrieveMultiple(QEindskr_iopresentation);

            activityPayload.MinorVersion = entities[0].GetAttributeValue<int>("indskr_minorversion");
            activityPayload.MajorVersion = entities[0].GetAttributeValue<int>("indskr_majorversion");
            activityPayload.CkmZipUrl = entities[0].GetAttributeValue<string>("indskr_ckmzipurl");
            activityPayload.CreatedBy = (entities[0].GetAttributeValue<EntityReference>("createdby")).Name;

            activityPayload.PageURL = new List<string>();


            foreach (var entity in entities.Entities)
            {
                var imageUrl = ((AliasedValue)entity["Pages.indskr_ckmpagethumbnailurl"]).Value.ToString();
                activityPayload.PageURL.Add(imageUrl);
            }

            AdaptiveCard adaptiveCard = _adaptiveCardCreator.CreateTeamsPresentationAdaptiveCard(activityPayload);
            return adaptiveCard;
        }
        #endregion
    }
}
