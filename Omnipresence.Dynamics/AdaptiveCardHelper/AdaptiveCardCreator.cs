﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdaptiveCards;
using Microsoft.Xrm.Sdk;
using Omnipresence.Dynamics.Model;

namespace Omnipresence.Dynamics.AdaptiveCardHelper
{
    public class AdaptiveCardCreator
    {
        public AdaptiveCard CreatePersonalAdaptiveCard(ApprovalActivityPayload value)
        {
            string filePath = Path.Combine(".", "Resources", "ApprovalPersonal.json");

            var adaptiveCardJson = File.ReadAllText(filePath);
            adaptiveCardJson = adaptiveCardJson.Replace("[ApprovalActivityId]", value.ApprovalActivityId);
            adaptiveCardJson = adaptiveCardJson.Replace("[TargetId]", value.TargetId);
            adaptiveCardJson = adaptiveCardJson.Replace("[TargetEntity]", value.TargetEntity);
            adaptiveCardJson = adaptiveCardJson.Replace("[Name]", value.Name);

            AdaptiveCardParseResult result = AdaptiveCard.FromJson(adaptiveCardJson);

            var version = new AdaptiveSchemaVersion(1, 0);
            AdaptiveCard card = new AdaptiveCard(version);
            card = result.Card;

            List<AdaptiveFact> adaptiveFactSets = ((AdaptiveFactSet)card.Body[1]).Facts;

            adaptiveFactSets[0].Value = value.Name;
            adaptiveFactSets[1].Value = value.Cycle;
            adaptiveFactSets[2].Value = value.ApprovalSequence;
            adaptiveFactSets[3].Value = value.Approver;
            adaptiveFactSets[4].Value = value.ApprovalMandatory ? "Yes" : "No";
            adaptiveFactSets[5].Value = value.ExpireIn;

            return card;
        }

        public AdaptiveCard CreateTeamsPresentationAdaptiveCard(PresentationAdaptivePayload value)
        {
            string filePath = Path.Combine(".", "Resources", "ApprovalActivityChannel.json");

            var adaptiveCardJson = File.ReadAllText(filePath);
            adaptiveCardJson = adaptiveCardJson.Replace("[TargetId]", value.TargetId);
            adaptiveCardJson = adaptiveCardJson.Replace("[TargetEntity]", value.TargetEntity);
            adaptiveCardJson = adaptiveCardJson.Replace("[Notification]", value.Name);
            adaptiveCardJson = adaptiveCardJson.Replace("[zipURL]", value.CkmZipUrl);

            AdaptiveCardParseResult result = AdaptiveCard.FromJson(adaptiveCardJson);

            var version = new AdaptiveSchemaVersion(1, 0);
            AdaptiveCard card = new AdaptiveCard(version);
            card = result.Card;

            List<AdaptiveFact> adaptiveFactSets = ((AdaptiveFactSet)card.Body[3]).Facts;

            adaptiveFactSets[0].Value = value.MajorVersion.ToString();
            adaptiveFactSets[1].Value = value.MinorVersion.ToString();
            adaptiveFactSets[2].Value = value.CreatedBy;

            foreach (var imageUrl in value.PageURL)
            {
                ((AdaptiveImageSet)card.Body[2]).Images.Add(new AdaptiveImage()
                {
                    Type = AdaptiveImage.TypeName,
                    Url = new Uri(imageUrl),
                    Size = AdaptiveImageSize.Large,
                    HorizontalAlignment = AdaptiveHorizontalAlignment.Center
                });
            }
            return card;
        }

        internal AdaptiveCard CreateTeamsQuoteAdaptiveCard(QuoteAdaptivePayload value)
        {
            string filePath = Path.Combine(".", "Resources", "ApprovalActivityQuoteChannel.json");

            var adaptiveCardJson = File.ReadAllText(filePath);
            adaptiveCardJson = adaptiveCardJson.Replace("[TargetId]", value.TargetId);
            adaptiveCardJson = adaptiveCardJson.Replace("[TargetEntity]", value.TargetEntity);
            adaptiveCardJson = adaptiveCardJson.Replace("[Notification]", value.Notification);


            AdaptiveCardParseResult result = AdaptiveCard.FromJson(adaptiveCardJson);

            var version = new AdaptiveSchemaVersion(1, 0);
            AdaptiveCard card = new AdaptiveCard(version);
            card = result.Card;

            List<AdaptiveFact> adaptiveFactSets = ((AdaptiveFactSet)card.Body[2]).Facts;

            adaptiveFactSets[0].Value = value.Name;
            adaptiveFactSets[1].Value = value.QuoteId;
            adaptiveFactSets[2].Value = value.PriceList;
            adaptiveFactSets[3].Value = value.RevisionId;
            adaptiveFactSets[4].Value = value.Currency;
            adaptiveFactSets[5].Value = value.CreatedBy;
            adaptiveFactSets[6].Value = value.TotalAmount;
            adaptiveFactSets[7].Value = value.DiscountAmount;
            adaptiveFactSets[8].Value = value.TotalTax;
            adaptiveFactSets[9].Value = value.TotalDetailAmount;

            return card;
        }

        
    }
}
