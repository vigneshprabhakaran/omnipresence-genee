﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: Microsoft.Xrm.Sdk.Client.ProxyTypesAssemblyAttribute()]



[System.Runtime.Serialization.DataContractAttribute()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "9.1.0.25")]
public enum indskr_approvalactivityState
{

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Active = 0,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Inactive = 1,
}

/// <summary>
/// 
/// </summary>
[System.Runtime.Serialization.DataContractAttribute()]
[Microsoft.Xrm.Sdk.Client.EntityLogicalNameAttribute("indskr_approvalactivity")]
[System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "9.1.0.25")]
public partial class indskr_approvalactivity : Microsoft.Xrm.Sdk.Entity, System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{

    /// <summary>
    /// Default Constructor.
    /// </summary>
    public indskr_approvalactivity() :
            base(EntityLogicalName)
    {
    }

    public const string EntityLogicalName = "indskr_approvalactivity";

    public const int EntityTypeCode = 11569;

    public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

    public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;

    private void OnPropertyChanged(string propertyName)
    {
        if ((this.PropertyChanged != null))
        {
            this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
        }
    }

    private void OnPropertyChanging(string propertyName)
    {
        if ((this.PropertyChanging != null))
        {
            this.PropertyChanging(this, new System.ComponentModel.PropertyChangingEventArgs(propertyName));
        }
    }

    /// <summary>
    /// Unique identifier of the user who created the record.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdby")]
    public Microsoft.Xrm.Sdk.EntityReference CreatedBy
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("createdby");
        }
    }

    /// <summary>
    /// Date and time when the record was created.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdon")]
    public System.Nullable<System.DateTime> CreatedOn
    {
        get
        {
            return this.GetAttributeValue<System.Nullable<System.DateTime>>("createdon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who created the record.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdonbehalfby")]
    public Microsoft.Xrm.Sdk.EntityReference CreatedOnBehalfBy
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("createdonbehalfby");
        }
    }

    /// <summary>
    /// Sequence number of the import that created this record.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("importsequencenumber")]
    public System.Nullable<int> ImportSequenceNumber
    {
        get
        {
            return this.GetAttributeValue<System.Nullable<int>>("importsequencenumber");
        }
        set
        {
            this.OnPropertyChanging("ImportSequenceNumber");
            this.SetAttributeValue("importsequencenumber", value);
            this.OnPropertyChanged("ImportSequenceNumber");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ind_presentationcatalog")]
    public Microsoft.Xrm.Sdk.EntityReference ind_PresentationCatalog
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ind_presentationcatalog");
        }
        set
        {
            this.OnPropertyChanging("ind_PresentationCatalog");
            this.SetAttributeValue("ind_presentationcatalog", value);
            this.OnPropertyChanged("ind_PresentationCatalog");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_accountcontactaffiliation")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_AccountContactAffiliation
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_accountcontactaffiliation");
        }
        set
        {
            this.OnPropertyChanging("indskr_AccountContactAffiliation");
            this.SetAttributeValue("indskr_accountcontactaffiliation", value);
            this.OnPropertyChanged("indskr_AccountContactAffiliation");
        }
    }

    /// <summary>
    /// Unique identifier for entity instances
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_approvalactivityid")]
    public System.Nullable<System.Guid> indskr_approvalactivityId
    {
        get
        {
            return this.GetAttributeValue<System.Nullable<System.Guid>>("indskr_approvalactivityid");
        }
        set
        {
            this.OnPropertyChanging("indskr_approvalactivityId");
            this.SetAttributeValue("indskr_approvalactivityid", value);
            if (value.HasValue)
            {
                base.Id = value.Value;
            }
            else
            {
                base.Id = System.Guid.Empty;
            }
            this.OnPropertyChanged("indskr_approvalactivityId");
        }
    }

    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_approvalactivityid")]
    public override System.Guid Id
    {
        get
        {
            return base.Id;
        }
        set
        {
            this.indskr_approvalactivityId = value;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_approvalitem")]
    public Microsoft.Xrm.Sdk.OptionSetValue indskr_ApprovalItem
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("indskr_approvalitem");
        }
        set
        {
            this.OnPropertyChanging("indskr_ApprovalItem");
            this.SetAttributeValue("indskr_approvalitem", value);
            this.OnPropertyChanged("indskr_ApprovalItem");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_approvalrequired")]
    public System.Nullable<bool> indskr_ApprovalRequired
    {
        get
        {
            return this.GetAttributeValue<System.Nullable<bool>>("indskr_approvalrequired");
        }
        set
        {
            this.OnPropertyChanging("indskr_ApprovalRequired");
            this.SetAttributeValue("indskr_approvalrequired", value);
            this.OnPropertyChanged("indskr_ApprovalRequired");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_approvalsequence")]
    public System.Nullable<int> indskr_ApprovalSequence
    {
        get
        {
            return this.GetAttributeValue<System.Nullable<int>>("indskr_approvalsequence");
        }
        set
        {
            this.OnPropertyChanging("indskr_ApprovalSequence");
            this.SetAttributeValue("indskr_approvalsequence", value);
            this.OnPropertyChanged("indskr_ApprovalSequence");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_approver")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_Approver
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_approver");
        }
        set
        {
            this.OnPropertyChanging("indskr_Approver");
            this.SetAttributeValue("indskr_approver", value);
            this.OnPropertyChanged("indskr_Approver");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_autoapproval")]
    public System.Nullable<int> indskr_AutoApproval
    {
        get
        {
            return this.GetAttributeValue<System.Nullable<int>>("indskr_autoapproval");
        }
        set
        {
            this.OnPropertyChanging("indskr_AutoApproval");
            this.SetAttributeValue("indskr_autoapproval", value);
            this.OnPropertyChanged("indskr_AutoApproval");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_contact")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_Contact
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_contact");
        }
        set
        {
            this.OnPropertyChanging("indskr_Contact");
            this.SetAttributeValue("indskr_contact", value);
            this.OnPropertyChanged("indskr_Contact");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_customeraddress")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_CustomerAddress
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_customeraddress");
        }
        set
        {
            this.OnPropertyChanging("indskr_CustomerAddress");
            this.SetAttributeValue("indskr_customeraddress", value);
            this.OnPropertyChanged("indskr_CustomerAddress");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_customerjourney")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_CustomerJourney
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_customerjourney");
        }
        set
        {
            this.OnPropertyChanging("indskr_CustomerJourney");
            this.SetAttributeValue("indskr_customerjourney", value);
            this.OnPropertyChanged("indskr_CustomerJourney");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_customerposition")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_CustomerPosition
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_customerposition");
        }
        set
        {
            this.OnPropertyChanging("indskr_CustomerPosition");
            this.SetAttributeValue("indskr_customerposition", value);
            this.OnPropertyChanged("indskr_CustomerPosition");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_cycle")]
    public System.Nullable<int> indskr_Cycle
    {
        get
        {
            return this.GetAttributeValue<System.Nullable<int>>("indskr_cycle");
        }
        set
        {
            this.OnPropertyChanging("indskr_Cycle");
            this.SetAttributeValue("indskr_cycle", value);
            this.OnPropertyChanged("indskr_Cycle");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_documentcatalog")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_DocumentCatalog
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_documentcatalog");
        }
        set
        {
            this.OnPropertyChanging("indskr_DocumentCatalog");
            this.SetAttributeValue("indskr_documentcatalog", value);
            this.OnPropertyChanged("indskr_DocumentCatalog");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_emailaddress")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_EmailAddress
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_emailaddress");
        }
        set
        {
            this.OnPropertyChanging("indskr_EmailAddress");
            this.SetAttributeValue("indskr_emailaddress", value);
            this.OnPropertyChanged("indskr_EmailAddress");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_event")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_Event
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_event");
        }
        set
        {
            this.OnPropertyChanging("indskr_Event");
            this.SetAttributeValue("indskr_event", value);
            this.OnPropertyChanged("indskr_Event");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_eventplan")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_eventplan
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_eventplan");
        }
        set
        {
            this.OnPropertyChanging("indskr_eventplan");
            this.SetAttributeValue("indskr_eventplan", value);
            this.OnPropertyChanged("indskr_eventplan");
        }
    }

    /// <summary>
    /// "Expiration Result" Option set defines what happens after Approval activity is expired.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_expirationresult")]
    public Microsoft.Xrm.Sdk.OptionSetValue indskr_ExpirationResult
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("indskr_expirationresult");
        }
        set
        {
            this.OnPropertyChanging("indskr_ExpirationResult");
            this.SetAttributeValue("indskr_expirationresult", value);
            this.OnPropertyChanged("indskr_ExpirationResult");
        }
    }

    /// <summary>
    /// Describes the action to perform once Escalated Approval Activity has expired.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_expiredescalationresult")]
    public Microsoft.Xrm.Sdk.OptionSetValue indskr_ExpiredEscalationResult
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("indskr_expiredescalationresult");
        }
        set
        {
            this.OnPropertyChanging("indskr_ExpiredEscalationResult");
            this.SetAttributeValue("indskr_expiredescalationresult", value);
            this.OnPropertyChanged("indskr_ExpiredEscalationResult");
        }
    }

    /// <summary>
    /// Internal Field to calculate to day the approval activity is set to expire.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_expiresin")]
    public System.Nullable<System.DateTime> indskr_ExpiresIn
    {
        get
        {
            return this.GetAttributeValue<System.Nullable<System.DateTime>>("indskr_expiresin");
        }
    }

    /// <summary>
    /// The name of the custom entity.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_name")]
    public string indskr_name
    {
        get
        {
            return this.GetAttributeValue<string>("indskr_name");
        }
        set
        {
            this.OnPropertyChanging("indskr_name");
            this.SetAttributeValue("indskr_name", value);
            this.OnPropertyChanged("indskr_name");
        }
    }

    /// <summary>
    /// There should be a link between the Manager approval activity and the original activity  There should be a field displayed on the activity form, and should be read-only
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_parentapprovalactivity")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_ParentApprovalActivity
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_parentapprovalactivity");
        }
        set
        {
            this.OnPropertyChanging("indskr_ParentApprovalActivity");
            this.SetAttributeValue("indskr_parentapprovalactivity", value);
            this.OnPropertyChanged("indskr_ParentApprovalActivity");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_pricelist")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_PriceList
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_pricelist");
        }
        set
        {
            this.OnPropertyChanging("indskr_PriceList");
            this.SetAttributeValue("indskr_pricelist", value);
            this.OnPropertyChanged("indskr_PriceList");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_reason")]
    public string indskr_Reason
    {
        get
        {
            return this.GetAttributeValue<string>("indskr_reason");
        }
        set
        {
            this.OnPropertyChanging("indskr_Reason");
            this.SetAttributeValue("indskr_reason", value);
            this.OnPropertyChanged("indskr_Reason");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_speaker")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_Speaker
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_speaker");
        }
        set
        {
            this.OnPropertyChanging("indskr_Speaker");
            this.SetAttributeValue("indskr_speaker", value);
            this.OnPropertyChanged("indskr_Speaker");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_speakercontract")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_SpeakerContract
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_speakercontract");
        }
        set
        {
            this.OnPropertyChanging("indskr_SpeakerContract");
            this.SetAttributeValue("indskr_speakercontract", value);
            this.OnPropertyChanged("indskr_SpeakerContract");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_speakerengagement")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_SpeakerEngagement
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_speakerengagement");
        }
        set
        {
            this.OnPropertyChanging("indskr_SpeakerEngagement");
            this.SetAttributeValue("indskr_speakerengagement", value);
            this.OnPropertyChanged("indskr_SpeakerEngagement");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_targetentityid")]
    public string indskr_TargetEntityId
    {
        get
        {
            return this.GetAttributeValue<string>("indskr_targetentityid");
        }
        set
        {
            this.OnPropertyChanging("indskr_TargetEntityId");
            this.SetAttributeValue("indskr_targetentityid", value);
            this.OnPropertyChanged("indskr_TargetEntityId");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_targetentityname")]
    public string indskr_TargetEntityName
    {
        get
        {
            return this.GetAttributeValue<string>("indskr_targetentityname");
        }
        set
        {
            this.OnPropertyChanging("indskr_TargetEntityName");
            this.SetAttributeValue("indskr_targetentityname", value);
            this.OnPropertyChanged("indskr_TargetEntityName");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_timeinreview")]
    public System.Nullable<int> indskr_TimeinReview
    {
        get
        {
            return this.GetAttributeValue<System.Nullable<int>>("indskr_timeinreview");
        }
        set
        {
            this.OnPropertyChanging("indskr_TimeinReview");
            this.SetAttributeValue("indskr_timeinreview", value);
            this.OnPropertyChanged("indskr_TimeinReview");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_triggertargetstatuschange")]
    public Microsoft.Xrm.Sdk.OptionSetValue indskr_TriggerTargetStatusChange
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("indskr_triggertargetstatuschange");
        }
        set
        {
            this.OnPropertyChanging("indskr_TriggerTargetStatusChange");
            this.SetAttributeValue("indskr_triggertargetstatuschange", value);
            this.OnPropertyChanged("indskr_TriggerTargetStatusChange");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_vendorengagement")]
    public Microsoft.Xrm.Sdk.EntityReference indskr_VendorEngagement
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("indskr_vendorengagement");
        }
        set
        {
            this.OnPropertyChanging("indskr_VendorEngagement");
            this.SetAttributeValue("indskr_vendorengagement", value);
            this.OnPropertyChanged("indskr_VendorEngagement");
        }
    }

    /// <summary>
    /// Unique identifier of the user who modified the record.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedby")]
    public Microsoft.Xrm.Sdk.EntityReference ModifiedBy
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("modifiedby");
        }
    }

    /// <summary>
    /// Date and time when the record was modified.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedon")]
    public System.Nullable<System.DateTime> ModifiedOn
    {
        get
        {
            return this.GetAttributeValue<System.Nullable<System.DateTime>>("modifiedon");
        }
    }

    /// <summary>
    /// Unique identifier of the delegate user who modified the record.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedonbehalfby")]
    public Microsoft.Xrm.Sdk.EntityReference ModifiedOnBehalfBy
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("modifiedonbehalfby");
        }
    }

    /// <summary>
    /// Date and time that the record was migrated.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("overriddencreatedon")]
    public System.Nullable<System.DateTime> OverriddenCreatedOn
    {
        get
        {
            return this.GetAttributeValue<System.Nullable<System.DateTime>>("overriddencreatedon");
        }
        set
        {
            this.OnPropertyChanging("OverriddenCreatedOn");
            this.SetAttributeValue("overriddencreatedon", value);
            this.OnPropertyChanged("OverriddenCreatedOn");
        }
    }

    /// <summary>
    /// Owner Id
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ownerid")]
    public Microsoft.Xrm.Sdk.EntityReference OwnerId
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ownerid");
        }
        set
        {
            this.OnPropertyChanging("OwnerId");
            this.SetAttributeValue("ownerid", value);
            this.OnPropertyChanged("OwnerId");
        }
    }

    /// <summary>
    /// Unique identifier for the business unit that owns the record
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("owningbusinessunit")]
    public Microsoft.Xrm.Sdk.EntityReference OwningBusinessUnit
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("owningbusinessunit");
        }
    }

    /// <summary>
    /// Unique identifier for the team that owns the record.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("owningteam")]
    public Microsoft.Xrm.Sdk.EntityReference OwningTeam
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("owningteam");
        }
    }

    /// <summary>
    /// Unique identifier for the user that owns the record.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("owninguser")]
    public Microsoft.Xrm.Sdk.EntityReference OwningUser
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("owninguser");
        }
    }

    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("statecode")]
    public System.Nullable<OptionSets.statecode> statecode
    {
        get
        {
            Microsoft.Xrm.Sdk.OptionSetValue attributeValue = this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("statecode");
            if ((attributeValue != null))
            {
                return ((OptionSets.statecode)(System.Enum.ToObject(typeof(OptionSets.statecode), attributeValue.Value)));
            }
            else
            {
                return null;
            }
        }
        set
        {
            this.OnPropertyChanging("statecode");
            if ((value == null))
            {
                this.SetAttributeValue("statecode", value);
            }
            else
            {
                this.SetAttributeValue("statecode", new Microsoft.Xrm.Sdk.OptionSetValue(((int)(value))));
            }
            this.OnPropertyChanged("statecode");
        }
    }

    /// <summary>
    /// Reason for the status of the Approval Activity
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("statuscode")]
    public Microsoft.Xrm.Sdk.OptionSetValue statuscode
    {
        get
        {
            return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("statuscode");
        }
        set
        {
            this.OnPropertyChanging("statuscode");
            this.SetAttributeValue("statuscode", value);
            this.OnPropertyChanged("statuscode");
        }
    }

    /// <summary>
    /// For internal use only.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("timezoneruleversionnumber")]
    public System.Nullable<int> TimeZoneRuleVersionNumber
    {
        get
        {
            return this.GetAttributeValue<System.Nullable<int>>("timezoneruleversionnumber");
        }
        set
        {
            this.OnPropertyChanging("TimeZoneRuleVersionNumber");
            this.SetAttributeValue("timezoneruleversionnumber", value);
            this.OnPropertyChanged("TimeZoneRuleVersionNumber");
        }
    }

    /// <summary>
    /// Time zone code that was in use when the record was created.
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("utcconversiontimezonecode")]
    public System.Nullable<int> UTCConversionTimeZoneCode
    {
        get
        {
            return this.GetAttributeValue<System.Nullable<int>>("utcconversiontimezonecode");
        }
        set
        {
            this.OnPropertyChanging("UTCConversionTimeZoneCode");
            this.SetAttributeValue("utcconversiontimezonecode", value);
            this.OnPropertyChanged("UTCConversionTimeZoneCode");
        }
    }

    /// <summary>
    /// Version Number
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("versionnumber")]
    public System.Nullable<long> VersionNumber
    {
        get
        {
            return this.GetAttributeValue<System.Nullable<long>>("versionnumber");
        }
    }

    /// <summary>
    /// 1:N indskr_approvalactivity_indskr_approvalactivity_ParentApprovalActivity
    /// </summary>
    [Microsoft.Xrm.Sdk.RelationshipSchemaNameAttribute("indskr_approvalactivity_indskr_approvalactivity_ParentApprovalActivity", Microsoft.Xrm.Sdk.EntityRole.Referenced)]
    public System.Collections.Generic.IEnumerable<indskr_approvalactivity> Referencedindskr_approvalactivity_indskr_approvalactivity_ParentApprovalActivity
    {
        get
        {
            return this.GetRelatedEntities<indskr_approvalactivity>("indskr_approvalactivity_indskr_approvalactivity_ParentApprovalActivity", Microsoft.Xrm.Sdk.EntityRole.Referenced);
        }
        set
        {
            this.OnPropertyChanging("Referencedindskr_approvalactivity_indskr_approvalactivity_ParentApprovalActivity");
            this.SetRelatedEntities<indskr_approvalactivity>("indskr_approvalactivity_indskr_approvalactivity_ParentApprovalActivity", Microsoft.Xrm.Sdk.EntityRole.Referenced, value);
            this.OnPropertyChanged("Referencedindskr_approvalactivity_indskr_approvalactivity_ParentApprovalActivity");
        }
    }

    /// <summary>
    /// N:1 indskr_approvalactivity_indskr_approvalactivity_ParentApprovalActivity
    /// </summary>
    [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("indskr_parentapprovalactivity")]
    [Microsoft.Xrm.Sdk.RelationshipSchemaNameAttribute("indskr_approvalactivity_indskr_approvalactivity_ParentApprovalActivity", Microsoft.Xrm.Sdk.EntityRole.Referencing)]
    public indskr_approvalactivity Referencingindskr_approvalactivity_indskr_approvalactivity_ParentApprovalActivity
    {
        get
        {
            return this.GetRelatedEntity<indskr_approvalactivity>("indskr_approvalactivity_indskr_approvalactivity_ParentApprovalActivity", Microsoft.Xrm.Sdk.EntityRole.Referencing);
        }
        set
        {
            this.OnPropertyChanging("Referencingindskr_approvalactivity_indskr_approvalactivity_ParentApprovalActivity" +
                    "");
            this.SetRelatedEntity<indskr_approvalactivity>("indskr_approvalactivity_indskr_approvalactivity_ParentApprovalActivity", Microsoft.Xrm.Sdk.EntityRole.Referencing, value);
            this.OnPropertyChanged("Referencingindskr_approvalactivity_indskr_approvalactivity_ParentApprovalActivity" +
                    "");
        }
    }

    public sealed class OptionSets
    {

        [System.Runtime.Serialization.DataContractAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "9.1.0.25")]
        public enum statecode
        {

            /// <summary>
            /// Active
            /// </summary>
            [System.Runtime.Serialization.EnumMemberAttribute()]
            Active = 0,

            /// <summary>
            /// Inactive
            /// </summary>
            [System.Runtime.Serialization.EnumMemberAttribute()]
            Inactive = 1,
        }
    }
}
