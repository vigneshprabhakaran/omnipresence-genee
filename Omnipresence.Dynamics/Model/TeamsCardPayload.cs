﻿using AdaptiveCards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnipresence.Dynamics.Model
{
    public class TeamsCardPayload
    {
        public UserDataEntity UserDataEntity { get; set; }

        public TeamDataEntity TeamDataEntity { get; set; }

        public AdaptiveCard AdaptiveCardPayload { get; set; }

        public bool IsChannelApproval { get; set; }


    }
}
