﻿using System.Collections.Generic;

namespace Omnipresence.Dynamics.Model
{
    public class PresentationAdaptivePayload
    {
        public string TargetId { get; set; }
        public string TargetEntity { get; set; }
        public string Name { get; set; }
        public string CkmZipUrl { get; set; }
        public int MinorVersion { get; set; }
        public int MajorVersion { get; set; }
        public string CreatedBy { get; set; }
        public List<string> PageURL { get; set; }
    }
}
