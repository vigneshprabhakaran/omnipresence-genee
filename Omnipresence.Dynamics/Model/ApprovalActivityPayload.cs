﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnipresence.Dynamics.Model
{
    public class ApprovalActivityPayload
    {
        public string ApprovalActivityId { get; set; }
        public string Name { get; set; }
        public string Cycle { get; set; }
        public string ApprovalSequence { get; set; }
        public string Approver { get; set; }
        public bool ApprovalMandatory { get; set; }
        public string ExpireIn { get; set; }
        public string Target { get; set; }
        public string TargetId { get; set; }
        public UserDataEntity UserDataEntity { get; set; }
        public string TargetEntity { get; set; }
    }
}
