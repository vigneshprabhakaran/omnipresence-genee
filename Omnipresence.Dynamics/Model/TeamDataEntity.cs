﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnipresence.Dynamics.Model
{
    public class TeamDataEntity
    {
        public string TeamId { get; set; }
        public string Name { get; set; }
        public string ServiceUrl { get; set; }
        public string TenantId { get; set; }
        public string ChannelId { get; set; }

    }
}
