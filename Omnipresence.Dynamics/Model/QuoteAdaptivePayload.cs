﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnipresence.Dynamics.Model
{
    public class QuoteAdaptivePayload
    {
        public string TargetId { get; set; }
        public string TargetEntity { get; set; }
        public string Notification { get; set; }
        public string Name { get; set; }
        public string QuoteId { get; set; }
        public string PriceList { get; set; }
        public string RevisionId { get; set; }
        public string Currency { get; set; }
        public string CreatedBy { get; set; }
        public string TotalAmount 	 { get; set; }
        public string DiscountAmount { get; set; }
        public string TotalTax { get; set; }
        public string TotalDetailAmount { get; set; }

    }
}
