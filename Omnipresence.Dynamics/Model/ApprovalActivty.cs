﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnipresence.Dynamics.Model
{
    public class ApprovalActivty
    {
        public string ApprovalActivityId { get; set; }
        public bool IsApproved { get; set; }
    }
}
