﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnipresence.Dynamics.Model
{
    public class UserDataEntity
    {
        public string AadId { get; set; }
        public string UserId { get; set; }
        public string ConversationId { get; set; }
        public string ServiceUrl { get; set; }
        public string TenantId { get; set; }
    }
}
