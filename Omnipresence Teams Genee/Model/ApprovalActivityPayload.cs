﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Omnipresence.TeamsGenee.Model
{
    public class ApprovalActivityPayload
    {
        public string ApprovalActivityId { get; set; }
    }
}
