﻿using AdaptiveCards;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Teams;
using Microsoft.Bot.Schema;
using Newtonsoft.Json;
using Omnipresence.Dynamics;
using Omnipresence.Dynamics.Model;
using Omnipresence.TeamsGenee.ConversationHelper;
using OmnipresenceTeamsGenee.Bot_Helper;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OmnipresenceTeamsGenee
{
    public class TeamsBot : TeamsActivityHandler
    {
        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            await BotHelper.SendTypingActivity(turnContext);

            if (turnContext.Activity.Value != null)
            {
                string Response = "Approval Activity Update Failed. Please Contact Admin.";

                var json = JsonConvert.SerializeObject(turnContext.Activity.Value);
                ApprovalActivty obj = JsonConvert.DeserializeObject<ApprovalActivty>(json);
                Response = obj.IsApproved ? "Approved" : "Not Approved";
                Attachment attachment = CreateThankYouCard(Response);
                var newActivity = MessageFactory.Attachment(attachment);
                await turnContext.SendActivityAsync(newActivity, cancellationToken);

                return;
            }

            return;
        }
        public static Attachment CreateThankYouCard(string message)
        {
            string filePath = Path.Combine(".", "Resources", "ApprovalCard.json");

            var adaptiveCardJson = File.ReadAllText(filePath);
            adaptiveCardJson = adaptiveCardJson.Replace("[status]", message);

            AdaptiveCardParseResult result = AdaptiveCard.FromJson(adaptiveCardJson);

            var version = new AdaptiveSchemaVersion(1, 0);
            AdaptiveCard card = new AdaptiveCard(version);
            card = result.Card;

            var adaptiveCardAttachment = new Attachment()
            {
                ContentType = "application/vnd.microsoft.card.adaptive",
                Content = card,
            };
            return adaptiveCardAttachment;
        }


        protected override async Task OnConversationUpdateActivityAsync(ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {

            var activity = turnContext.Activity;
            var botId = activity.Recipient.Id;
            await base.OnConversationUpdateActivityAsync(turnContext, cancellationToken);

            if (activity.MembersAdded?.FirstOrDefault(p => p.Id == botId) != null)
            {
                ConversationReferenceHelper.OnBotAddedAsync(activity);
            }
        }
    }
}
