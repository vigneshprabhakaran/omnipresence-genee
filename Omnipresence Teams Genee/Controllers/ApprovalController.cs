﻿using AdaptiveCards;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Integration.AspNet.Core;
using Microsoft.Bot.Connector.Authentication;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Omnipresence.Dynamics.Interface;
using Omnipresence.Dynamics.Model;
using Omnipresence.TeamsGenee.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Omnipresence.TeamsGenee.Controllers
{
    [Route("api/approval")]
    [ApiController]
    public class ApprovalController
    {
        private readonly IApprovalActivity approvalInterface;
        private readonly ConversationReference conversationRef;
        private readonly IBotFrameworkHttpAdapter _adapter;
        private readonly string _appId;

        private static readonly string MsTeamsChannelId = "msteams";
        private const string PersonalType = "personal";
        private const string ChannelType = "channel";

        public ApprovalController(IBotFrameworkHttpAdapter adapter, IConfiguration configuration)
        {
            approvalInterface = new IApprovalActivity();
            conversationRef = new ConversationReference();
            _adapter = adapter;
            _appId = configuration["MicrosoftAppId"];
        }

        public Attachment ApprovalPayload { get; set; }

        [HttpPost]
        [HttpPost("PostApprovalActivity")]
        public async Task<IActionResult> PostApprovalActivity([FromBody] Dynamics.Model.ApprovalActivityPayload value)
        {
            Guid approvalActivityId = new Guid(value.ApprovalActivityId);
            TeamsCardPayload approvalAdaptiveCardPayload = approvalInterface.GetApprovalAdaptiveCard(approvalActivityId);
            if (approvalAdaptiveCardPayload.AdaptiveCardPayload != null)
            {
                var adaptiveCardAttachment = new Attachment()
                {
                    ContentType = "application/vnd.microsoft.card.adaptive",
                    Content = approvalAdaptiveCardPayload.AdaptiveCardPayload,
                };
                ApprovalPayload = adaptiveCardAttachment;

                if (!approvalAdaptiveCardPayload.IsChannelApproval)
                {
                    return await NotifyUser(approvalAdaptiveCardPayload);
                }
                else
                {
                    return await NotifyTeams(approvalAdaptiveCardPayload);
                }
            }
            else
            {
                return new ContentResult()
                {
                    StatusCode = (int)HttpStatusCode.OK
                };
            }
        }

        private async Task<IActionResult> NotifyTeams(TeamsCardPayload approvalAdaptiveCardPayload)
        {
            TeamDataEntity teamDataEntity = approvalAdaptiveCardPayload.TeamDataEntity;
            ConversationReference conversation = PrepareConversationReferenceForTeamsAsync(teamDataEntity);

            if (conversation != null)
            {
                await ((BotAdapter)_adapter).ContinueConversationAsync(_appId, conversation, SendApprovalToTeam, default);
                return new ContentResult()
                {
                    StatusCode = (int)HttpStatusCode.OK
                };
            }
            else
            {
                return new ContentResult()
                {
                    StatusCode = (int)HttpStatusCode.PreconditionFailed
                };
            }
        }

        private async Task SendApprovalToTeam(ITurnContext turnContext, CancellationToken cancellationToken)
        {
            try
            {
                var reply = MessageFactory.Attachment(ApprovalPayload);

                MicrosoftAppCredentials.TrustServiceUrl(turnContext.Activity.ServiceUrl);
                await turnContext.SendActivityAsync(reply);
            }
            catch (Exception ex)
                {

                throw;
            }
            
        }

        private ConversationReference PrepareConversationReferenceForTeamsAsync(TeamDataEntity teamDataEntity)
        {
            var channelAccount = new ChannelAccount
            {
                Id = $"28:{_appId}",
            };

            var conversationAccount = new ConversationAccount
            {
                ConversationType = ChannelType,
                Id = teamDataEntity.TeamId,
                TenantId = teamDataEntity.TenantId,
            };

            var conversationReference = new ConversationReference
            {
                Bot = channelAccount,
                ChannelId = MsTeamsChannelId,
                Conversation = conversationAccount,
                ServiceUrl = teamDataEntity.ServiceUrl,
            };

            return conversationReference;
        }

        private async Task<IActionResult> NotifyUser(TeamsCardPayload approvalAdaptiveCardPayload)
        {
            UserDataEntity userDataEntity = approvalAdaptiveCardPayload.UserDataEntity;
            ConversationReference conversation = PrepareConversationReferenceForUsersAsync(userDataEntity);
            if (conversation != null)
            {
                await ((BotAdapter)_adapter).ContinueConversationAsync(_appId, conversation, SendApprovalToPersonal, default);
                return new ContentResult()
                {
                    StatusCode = (int)HttpStatusCode.OK
                };
            }
            else
            {
                return new ContentResult()
                {
                    StatusCode = (int)HttpStatusCode.PreconditionFailed
                };
            }
        }

        private ConversationReference PrepareConversationReferenceForUsersAsync(UserDataEntity userDataEntity)
        {
            var channelAccount = new ChannelAccount
            {
                Id = $"28:{_appId}"

            };

            var user = new ChannelAccount
            {
                Id = userDataEntity.UserId,
                AadObjectId = userDataEntity.AadId
            };

            var conversationAccount = new ConversationAccount
            {
                ConversationType = PersonalType,
                Id = userDataEntity.ConversationId,
                TenantId = userDataEntity.TenantId,
            };

            var conversationReference = new ConversationReference
            {
                Bot = channelAccount,
                ChannelId = MsTeamsChannelId,
                Conversation = conversationAccount,
                ServiceUrl = userDataEntity.ServiceUrl,
                User = user
            };

            return conversationReference;
        }

        private async Task SendApprovalToPersonal(ITurnContext turnContext, CancellationToken cancellationToken)
        {
            var reply = MessageFactory.Attachment(ApprovalPayload);

            MicrosoftAppCredentials.TrustServiceUrl(turnContext.Activity.ServiceUrl);
            try
            {
                await turnContext.SendActivityAsync(reply);
            }
            catch (Exception ex)
            {
                throw;
            }  
            
        }
    }
}
