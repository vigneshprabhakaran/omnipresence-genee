﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmnipresenceTeamsGenee.Bot_Helper
{
    public static class BotHelper
    {
        public static async Task SendTypingActivity(ITurnContext<IMessageActivity> turnContext)
        {
            var reply = Activity.CreateTypingActivity();
            await turnContext.SendActivityAsync(reply);
        }
    }
}
