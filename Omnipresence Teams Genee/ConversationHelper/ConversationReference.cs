﻿using Microsoft.Bot.Schema;
using Microsoft.Bot.Schema.Teams;
using Omnipresence.Dynamics.Interface;
using Omnipresence.Dynamics.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Omnipresence.TeamsGenee.ConversationHelper
{
    public static class ConversationReferenceHelper
    {
        private const string PersonalType = "personal";
        private const string ChannelType = "channel";

        public static void OnBotAddedAsync(IConversationUpdateActivity activity)
        {
            switch (activity.Conversation.ConversationType)
            {
                case ConversationReferenceHelper.ChannelType:
                    var teamDataEntity = ParseTeamData(activity);
                    if (teamDataEntity != null)
                    {
                        ITeamDataRepository.CreateOrUpdateAsync(teamDataEntity);
                    }
                    break;

                case ConversationReferenceHelper.PersonalType:
                    var userDataEntity = ParseUserData(activity);
                    if (userDataEntity != null)
                    {
                        IUserDataRepository.CreateOrUpdateAsync(userDataEntity);
                    }
                    break;

                default: break;
            }
        }

        private static UserDataEntity ParseUserData(IConversationUpdateActivity activity)
        {
            var rowKey = activity?.From?.AadObjectId;
            if (rowKey != null)
            {
                var userDataEntity = new UserDataEntity
                {
                    AadId = activity?.From?.AadObjectId,
                    UserId = activity?.From?.Id,
                    ConversationId = activity?.Conversation?.Id,
                    ServiceUrl = activity?.ServiceUrl,
                    TenantId = activity?.Conversation?.TenantId,
                };

                return userDataEntity;
            }

            return null;
        }

        private static TeamDataEntity ParseTeamData(IConversationUpdateActivity activity)
        {
            var channelData = activity.GetChannelData<TeamsChannelData>();
            if (channelData != null)
            {
                var teamsDataEntity = new TeamDataEntity
                {
                    TeamId = channelData.Team.Id,
                    Name = channelData.Team.Name,
                    ServiceUrl = activity.ServiceUrl,
                    TenantId = channelData.Tenant.Id,
                    ChannelId = ((Activity)activity).From.Id
                };

                return teamsDataEntity;
            }

            return null;
        }
    }
}
